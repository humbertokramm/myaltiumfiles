
@echo off
call variavel.bat
cls


setlocal enabledelayedexpansion

rem Substitua pelo caminho do diretório desejado
set "diretorio_alvo=C:\Users\humberto.kramm\AppData\Local\Temp"
set "extensao_desejada=.bin"
set "prefixo_desejado=arduino_build"
set CurrentDir=%cd%
set ProjetosEspeciais=P:\PCP\Firmware\Especiais
set Projetos=P:\PCP\Firmware
set atalho=B:\Config\atalho

set Parametro=%1
set Comando=%2
if "%1"=="" set Parametro=FALSE
if "%2"=="" set Comando=FALSE



::if %Parametro% == c call echo c
::if %Parametro% == C call echo C
::if %Parametro% == r call echo r


:: Cria o arquivo de versão
set atalho=B:\Config\atalho
copy %atalho%\versao.txt
echo //	Projeto: %NomeProjeto% >> versao.txt
echo // >> versao.txt
git log --date=format:"%%d-%%m-%%Y" --pretty=format:"%%cd - %%s" >> versao.txt


if %Comando% == s (
	echo Linha
	if not exist "%Projetos%\%Linha%\%NomeProjeto%" (
		echo criado
		echo "%Projetos%\%Linha%\%NomeProjeto%"
		MD "%Projetos%\%Linha%\%NomeProjeto%"
	) else (
		::Cópia do ultimo arquivo
		CD "%Projetos%\%Linha%\%NomeProjeto%"
		if not exist "%Projetos%\%Linha%\%NomeProjeto%\OLD" (
			echo criado pasta de backup
			echo "%Projetos%\%Linha%\%NomeProjeto%\OLD"
			MD "%Projetos%\%Linha%\%NomeProjeto%\OLD"
		)
		
		for %%I in ("%Projetos%\%Linha%\%NomeProjeto%\*ino.bin") do (
			set "namefile=%%~nxI"
			echo %%~nxI
			
			set "data_hora=%%~tI"
			echo %%~tI
			set "minuto=!data_hora:~14,2!"
			set "hora=!data_hora:~11,2!"
			set "ano=!data_hora:~6,4!"
			set "mes=!data_hora:~3,2!"
			set "dia=!data_hora:~0,2!"
			set "data_formatada=[ !ano!-!mes!-!dia! !hora!h!minuto!m ]
			echo Data: !data_formatada!
		)
		7z a -tzip "%NomeProjeto% !data_formatada!.zip" *.bin *.txt
		move "*.zip" "%Projetos%\%Linha%\%NomeProjeto%\OLD"
		CD "%CurrentDir%"
	)
	for /f "delims=" %%I in ('dir /b /ad-h /od "%diretorio_alvo%\%prefixo_desejado%*"') do (
		set "nameprojet=%%~nxI"
	)
	for %%I in ("%diretorio_alvo%\!nameprojet!\*%extensao_desejada%") do (
		set "namefile=%%~nxI"
		copy %diretorio_alvo%\!nameprojet!\!namefile! "%Projetos%\%Linha%\%NomeProjeto%"
	)
	
	for %%I in ("%diretorio_alvo%\!nameprojet!\*%extensao_desejada%") do (
		set "namefile=%%~nxI"
		copy %diretorio_alvo%\!nameprojet!\!namefile! "%Projetos%\%Linha%\%NomeProjeto%"
	)
	
	move versao.txt "%Projetos%\%Linha%\%NomeProjeto%"
	copy variavel.bat "%Projetos%\%Linha%\%NomeProjeto%"
	set "variable=!namefile:.ino.partitions.bin=!"
	echo set NomeArquivo=!variable!>>"%Projetos%\%Linha%\%NomeProjeto%\variavel.bat"
	copy %atalho%\ProgESP.bat "%Projetos%\%Linha%\%NomeProjeto%
)


endlocal







