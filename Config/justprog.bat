cls
@echo off
call variavel.bat

cls

if exist "COMPILADO\%NomeArquivo%.hex" (
	color F9
	if %PowerPic% == 3V (
		PK3CMD -P%Pic% -E -FCOMPILADO\%NomeArquivo%.Hex -M -L -V3.000
	) else if %PowerPic% == 5V (
		::PK3CMD -P%Pic% -E -V5.000
		PK3CMD -P%Pic% -E -FCOMPILADO\%NomeArquivo%.Hex -M -L -V5.000
	) else if %PowerPic% == 4.75V (
		PK3CMD -P%Pic% -E -FCOMPILADO\%NomeArquivo%.Hex -M -L -V4.750
	) else if %PowerPic% == FALSE (
		PK3CMD -P%Pic% -E -FCOMPILADO\%NomeArquivo%.Hex -M -L 
	) else (
		echo .
		echo .
		echo .
		echo !!!!Erro de sintaxe na Alimentacao!!!!
		echo .
		echo .
		echo .
	)
	if %ForceReset% == TRUE (
		PK3CMD -P%Pic% -L
	) 
	del *.0
	if exist *.1 (
		del *.1
	)
	del *.xml
	del *.lck
	color F2
)
if not exist "COMPILADO\%NomeArquivo%.hex" (
	color FC
)