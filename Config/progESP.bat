@echo off
call variavel.bat
cls

setlocal enabledelayedexpansion

set n=0

set SERIAL=nan
for /f "tokens=1* delims==" %%I in ('wmic path win32_pnpentity get caption /format:list ^| find "USB" ^| find "(COM"') do (
	::echo "%%~J"
    call :setCOM "%%~J"
)
goto :DIRE


:setCOM <WMIC_output_line>
set "str=%~1"
set "num=%str:*(COM=%"
set "num=%num:)=%"
set  SERIAL=COM%num%
echo %SERIAL%

goto :end
:DIRE
if %SERIAL%==nan (
echo/
set SERIAL=COM7
echo/
echo/
echo     Porta Serial desconectada, verifique o gravador!
echo/
echo/
echo     Tentando gravar pela COM7
echo/
echo/
echo/
pause
goto :grava
goto :end


:grava
set partitions=%userprofile%\AppData\Local\Arduino15\packages\esp32\hardware\esp32\1.0.6/tools/partitions/boot_app0.bin
set bootloader=%userprofile%\AppData\Local\Arduino15\packages\esp32\hardware\esp32\1.0.6/tools/sdk/bin/bootloader_dio_80m.bin
cls

if not exist "OLD\" (
	echo Log criado
	MD "OLD"
)

echo -------------------------
echo Projeto: %NomeProjeto%
echo MCU: ESP32
echo Nome do Firmware: %NomeArquivo%
echo -------------------------

esptool.exe --chip esp32 --port %SERIAL% --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0xe000 %partitions% 0x1000 %bootloader% 0x10000 %NomeArquivo%.ino.bin 0x8000 %NomeArquivo%.ino.partitions.bin 

set /A n+=1
echo Gravado em %date% - %time% por %username% n:%n%>> OLD/log.txt

echo/
echo/
echo/
echo/
echo     %n% placas gravadas com %NomeProjeto%
echo/
echo     Pressione "q" para sair ou "enter" para continuar
echo/
echo/
echo/

set /p Input= 
if /I "%Input%"=="q" goto :end

goto :grava

:end


endlocal