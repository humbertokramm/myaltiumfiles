::cls
@echo on

call compila.bat
call variavel.bat

if %PowerPic% == STANDAR (
	set PowerPic=4.50V
	)

echo -------------------------
echo Projeto: %NomeProjeto%
echo MCU: PIC %Pic% 
if %PowerPic% == FALSE (
	echo Usando alimentacao Externa
	)else echo o gravador vai alimentar com %PowerPic%
echo Nome do Firmware: %NomeArquivo%.hex
echo -------------------------

if exist "COMPILADO\%NomeArquivo%.hex" (
	color F9
	if %PowerPic% == 3V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V3.000
	) else if %PowerPic% == 5V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V5.000
	) else if %PowerPic% == 4.75V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V4.750
	) else if %PowerPic% == 4.50V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V4.500
	) else if %PowerPic% == 4V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V4.000
	) else if %PowerPic% == 3.30V (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L -V3.300
	) else if %PowerPic% == FALSE (
		PK3CMD -P%Pic% -FCOMPILADO\%NomeArquivo%.Hex -M -L 
	) else (
		echo .
		echo .
		echo .
		echo !!!!Erro de sintaxe na Alimentacao!!!!
		echo .
		echo .
		echo .
	)
	if %ForceReset% == TRUE (
		PK3CMD -P%Pic% -L
	) 
	if exist *.0 (
		del *.0
	)
	if exist *.1 (
		del *.1
	)
	if exist *.2 (
		del *.2
	)
	if exist *.3 (
		del *.3
	)
	if exist *.4 (
		del *.4
	)
	if exist *.5 (
		del *.5
	)
	if exist *.6 (
		del *.6
	)
	if exist *.7 (
		del *.7
	)
	if exist *.8 (
		del *.8
	)
	if exist *.9 (
		del *.9
	)
	if exist *.10 (
		del *.10
	)
	if exist *.xml (
		del *.xml
	)
	if exist *.lck (
		del *.lck
	)
	color F2
)
if not exist "COMPILADO\%NomeArquivo%.hex" (
	color FC
)