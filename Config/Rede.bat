@echo off
REM ****************************************************************************
REM *  Bath para abrir pastas automaticamente baseado em parâmetros            *
REM *  by Gilberto Almeida                                                     *
REM *  17/07/2021 - melhorias nas mensagens de erro                            * 
REM *             - previsão para abertura de pastas home (uso descontado)     *
REM *  13/05/2021 - previsão para abertura da pasta de figuras                 *
REM *  07/04/2021 - previsão para abertura da pasta Public                     *
REM *  28/01/2021 - aumentado tempo entre aberturas explorer opção 'job'       *
REM *  20/01/2021 - aumentado tempo entre aberturas explorer opção 'job'       *
REM *  20/01/2021 - criação                                                    *
REM ****************************************************************************

if "%1"=="" goto ErroVazio
if "%1"=="job" goto AbreJob
if "%1"=="trocas" goto AbreTrocas
if "%1"=="pub" goto AbrePub
if "%1"=="fig" goto AbreFig
if "%1"=="home" goto AbreHome
if "%2"=="" goto ErroVazio
if "%3"=="" goto ErroVazio

REM Abre pastas de documentos codificados
REM estrutura antiga
@echo Pesquisando por %1.%2.%3...
if exist f:\dat\document\produtos\sincronismo\interno\%1%2%3 explorer f:\dat\document\produtos\sincronismo\interno\%1%2%3
if exist f:\dat\document\produtos\interno\%1%2%3 explorer f:\dat\document\produtos\interno\%1%2%3
if exist f:\dat\document\produtos\restrito\%1%2%3 explorer f:\dat\document\produtos\restrito\%1%2%3
if exist f:\dat\document\produtos\sincronismo\restrito\%1%2%3 explorer f:\dat\document\produtos\sincronismo\restrito\%1%2%3
if exist F:\DAT\DOCUMENT\Produtos\Sincronismo\Morto\Interno\%1%2%3 explorer F:\DAT\DOCUMENT\Produtos\Sincronismo\Morto\Interno\%1%2%3

REM estrutura nova (GED)
if "%4"=="" goto End
@echo Pesquisando por %1.%2.%3-%4...
if exist F:\DAT\DOCUMENT\GED\PRODUTOS\EDICAO\%1.%2.%3-%4 explorer F:\DAT\DOCUMENT\GED\PRODUTOS\EDICAO\%1.%2.%3-%4
if exist F:\DAT\DOCUMENT\GED\PRODUTOS\INTERNO\%1.%2.%3-%4 explorer F:\DAT\DOCUMENT\GED\PRODUTOS\INTERNO\%1.%2.%3-%4
if exist F:\DAT\DOCUMENT\GED\PRODUTOS\RESTRITO\%1.%2.%3-%4 explorer F:\DAT\DOCUMENT\GED\PRODUTOS\RESTRITO\%1.%2.%3-%4
if exist F:\DAT\DOCUMENT\GED\PRODUTOS\MORTO\%1.%2.%3-%4 explorer F:\DAT\DOCUMENT\GED\PRODUTOS\MORTO\%1.%2.%3-%4

if "%5"=="" goto End
@echo Pesquisando por %1.%2.%3-%4_%5...
if exist F:\DAT\DOCUMENT\GED\PRODUTOS\RESTRITO\%1.%2.%3-%4_%5 explorer F:\DAT\DOCUMENT\GED\PRODUTOS\RESTRITO\%1.%2.%3-%4_%5
goto End

REM *** Subrotinas ***
:AbreJob
explorer c:\gilberto
timeout /T 5
explorer c:\temp
timeout /T 5
explorer f:\
timeout /T 9
explorer f:\
goto End

:AbreTrocas
if not exist f:\dat\areas\trocas goto ErroF
if not exist f:\dat\areas\trocas\Gilberto md f:\dat\areas\trocas\Gilberto
explorer f:\dat\areas\trocas\Gilberto
goto End

:AbrePub
if not exist f:\dat\document\produtos\public goto ErroF
explorer f:\dat\document\produtos\public
goto End

:AbreFig
if not exist F:\DAT\DOCUMENT\BIBLIOTECA\89100_Fotografias_Figuras_Desenhos goto ErroF
explorer F:\DAT\DOCUMENT\BIBLIOTECA\89100_Fotografias_Figuras_Desenhos
goto End

:AbreHome
if not exist g:\ goto ErroPendrive
explorer g:\
timeout /T 5
explorer c:\temp
goto End

REM *** Mensagens de erro ***
:ErroVazio
@echo.
@echo ********************************************************************************************************
@echo * Sintaxe possivel:                                                                                    *
@echo *   Rede job              (abre pastas de trabalho com tempo entre elas)                               *
@echo *   Rede trocas           (cria/abre a pasta f:\dat\areas\trocas\Gilberto)                              *
@echo *   Rede pub              (abre a pasta f:\dat\document\produtos\public)                               *
@echo *   Rede fig              (abre a pasta F:\DAT\DOCUMENT\BIBLIOTECA\89100_Fotografias_Figuras_Desenhos) *
@echo *   Rede 01 400 669       (abre pastas na estrutura antiga)                                            *
@echo *   Rede 01 400 669 4 FA  (abre pastas na estrutura antiga e nova)                                     *
@echo ********************************************************************************************************
@echo.
pause
goto End

:ErroF
@echo Drive F: não encontrado
pause
goto End

:ErroPendrive
@echo Pendrive não encontrado
pause

:End