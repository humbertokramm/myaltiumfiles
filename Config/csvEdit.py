import os,webbrowser,subprocess
import re
import sys
from pprint import pprint


Words = [
	['TopLayer','T'],
	['BottomLayer','B'],
	['Res ',''],
	['Ind.Cer ',''],
	['Cap.Cer. ',''],
	['Cap.Tant. ',''],
	['Cap.Elet. ',''],
	['Cap.Poli. ',''],
	[' / ','/'],
	['_-_micro',''],
	['º',''],
	[' ','_'],
]

PTHlist = [
	'AXIAL',
	'STLZ',
	'Comprimento',
	'ICSP',
	'CAP._POL._18_x_10',
	'5046-0',
	'Header',
	'41792-',
	'5028-',
	'ESPAÇADOR',
	'TRAFO_PQ',
	'Fuse',
	'DIODE_DO-15',
	'DIODE6',
	'RES_3W',
	'TO-220',
	'VARISTOR-_DIA_9.8MM',
	'KBL',
	'CAPPR',
	'rele_SRA',
	'JACK',
	'TERRA',
]

def handleLine(L):
	for word in Words:
		L = L.replace(word[0],word[1])
	return L

def notPTH(L):
	for pth in PTHlist:
		if L.find(pth) > 0:
			print('Linha removida --> '+L[:-1])
			return False
	return True
	
	
def commaTodot(L):
	index = 0;
	#Busca por mais de 6 vírgulas
	for i in range(7):
		index = L.find(',',index+1)
	#caso haja mais de 6 vírgulas...
	if index > 0:
		#separa o intervalo
		strV = L[index-1:index+2]
		#troca o intervalo para ponto
		NewstrV = strV.replace(',','.')
		#mostra a correção
		print('troca de vírugula para ponto em: '+L[index-2:index+3])
		#substitui na linha
		L = L.replace(strV,NewstrV)
		#print(strV)
	index = 0;
	#Busca novamente por mais de 6 vírgulas
	for i in range(7):
		index = L.find(',',index+1)
	if index > 0:
		print('existe mais de 7 vírgulas nessa linha:')
		print(L)
	return L

def selectLayer():
	x = ''
	while x != '1' and x != '2' and x.lower() != 'q':
		print('\n\n\n Selecione o layer para a pick and place:')
		print('\t(1) Top Layer')
		print('\t(2) Botton Layer')
		print('\t(q) sair')
		x = input()
	if x == '1': PTHlist.append('BottonLayer')
	if x == '2': PTHlist.append('TopLayer')


try:
	selectLayer()
	files = os.listdir()
	file = ''
	for f in files:
		if f[-4:].lower() == '.csv':
			file = f

	with open(file, 'r') as fr:
		lines = fr.readlines()
		with open(file[11:], 'w') as fw:
			saveLine = False
			for line in lines:
				if line[1:11] == 'Designator':
					saveLine = True
				if saveLine:
					if notPTH(line):
						line = handleLine(line)
						line = commaTodot(line)
						fw.write(handleLine(line))
	print("Deleted Header")
except:
	print("Oops! something error")



