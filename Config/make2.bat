
@echo off
cls

set datef=%date:~-4%_%date:~3,2%_%date:~0,2%
set timef=%time:~0,2%horas
set Parametro=%1
set Comando=%2
if "%1"=="" set Parametro=FALSE
if "%2"=="" set Comando=FALSE


set CurrentDir=%cd%
set ProjetosEspeciais=P:\PCP\Firmware\Especiais
set Nuvem=D:\nuvens\onedrive-H\OneDrive\PI\Firmware
set Projetos=P:\PCP\Firmware
set atalho=B:\Config\atalho

call variavel.bat


if %Parametro% == c call compila.bat
if %Parametro% == C call compila18.bat
if %Parametro% == r call read.bat

if %Comando% == b (
	7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip"  COMPILADO\*.hex *.txt *.c *.h
	CD..
	move "%NomeProjeto%\%NomeProjeto% %datef% ; %timef%.zip"
	D:
	CD %Nuvem%
	if not exist "%NomeProjeto%" (
		echo criado
		MD "%NomeProjeto%"
	)
	p:
	copy "%NomeProjeto% %datef% ; %timef%.zip" "%Nuvem%\%NomeProjeto%\"
	cd %NomeProjeto%
)
	
if %Comando% == s (
	7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip"  COMPILADO\*.hex *.txt *.c *.h
	CD..
	move "%CurrentDir%\%NomeProjeto% %datef% ; %timef%.zip"
	cd %CurrentDir%
	
	if %ProjetoEspecial% == TRUE (
		echo Especial
		if not exist "%ProjetosEspeciais%\%NomeProjeto%" (
			echo criado
			echo "%ProjetosEspeciais%\%NomeProjeto%"
			MD "%ProjetosEspeciais%\%NomeProjeto%"
		) else (
			::C�pia do ultimo arquivo
			CD "%ProjetosEspeciais%\%NomeProjeto%"
			if not exist "%ProjetosEspeciais%\%NomeProjeto%\OLD" (
				echo criado pasta de backup
				echo "%ProjetosEspeciais%\%NomeProjeto%\OLD"
				MD "%ProjetosEspeciais%\%NomeProjeto%\OLD"
			)
			7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip" *.hex *.txt
			
			move "*.zip" "%ProjetosEspeciais%\%NomeProjeto%\OLD"
			CD "%CurrentDir%"
		)
		copy COMPILADO\%NomeArquivo%.hex "%ProjetosEspeciais%\%NomeProjeto%"
		copy versao.txt "%ProjetosEspeciais%\%NomeProjeto%"
		copy variavel.bat "%ProjetosEspeciais%\%NomeProjeto%"
		copy Info.xlsx "%ProjetosEspeciais%\%NomeProjeto%"
		copy %atalho%\Prog.bat "%ProjetosEspeciais%\%NomeProjeto%"
		copy %atalho%\Read.bat "%ProjetosEspeciais%\%NomeProjeto%"
	)
	if %ProjetoEspecial% == FALSE (
		echo Linha
		if not exist "%Projetos%\%Linha%\%NomeProjeto%" (
			echo criado
			echo "%Projetos%\%Linha%\%NomeProjeto%"
			MD "%Projetos%\%Linha%\%NomeProjeto%"
		) else (
			::C�pia do ultimo arquivo
			CD "%Projetos%\%Linha%\%NomeProjeto%"
			if not exist "%Projetos%\%Linha%\%NomeProjeto%\OLD" (
				echo criado pasta de backup
				echo "%Projetos%\%Linha%\%NomeProjeto%\OLD"
				MD "%Projetos%\%Linha%\%NomeProjeto%\OLD"
			)
			7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip" *.hex *.txt
			
			move "*.zip" "%Projetos%\%Linha%\%NomeProjeto%\OLD"
			CD "%CurrentDir%"
		)

		copy COMPILADO\%NomeArquivo%.hex "%Projetos%\%Linha%\%NomeProjeto%"
		copy versao.txt "%Projetos%\%Linha%\%NomeProjeto%"
		copy variavel.bat "%Projetos%\%Linha%\%NomeProjeto%"
		copy Info.xlsx "%Projetos%\%Linha%\%NomeProjeto%"
		copy %atalho%\Prog.bat "%Projetos%\%Linha%\%NomeProjeto%"
		copy %atalho%\Read.bat "%Projetos%\%Linha%\%NomeProjeto%"
	)
)
if %Comando% == S (
	7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip"  COMPILADO\*.hex *.txt *.c *.h
	CD..
	move "%NomeProjeto%\%NomeProjeto% %datef% ; %timef%.zip"
	cd %NomeProjeto%
	
	if %ProjetoEspecial% == TRUE (
		echo Especial
		if not exist "%ProjetosEspeciais%\%NomeProjeto%-Beta" (
			echo criado
			echo "%ProjetosEspeciais%\%NomeProjeto%-Beta"
			MD "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		) else (
			::C�pia do ultimo arquivo
			CD "%ProjetosEspeciais%\%NomeProjeto%-Beta"
			if not exist "%ProjetosEspeciais%\%NomeProjeto%-Beta\OLD" (
				echo criado pasta de backup
				echo "%ProjetosEspeciais%\%NomeProjeto%-Beta\OLD"
				MD "%ProjetosEspeciais%\%NomeProjeto%-Beta\OLD"
			)
			7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip" *.hex *.txt
			
			move "*.zip" "%ProjetosEspeciais%\%NomeProjeto%-Beta\OLD"
			CD "%CurrentDir%"
		)
		copy COMPILADO\%NomeArquivo%.hex "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		copy versao.txt "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		copy variavel.bat "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		copy Info.xlsx "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		copy %atalho%\Prog.bat "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		copy %atalho%\Read.bat "%ProjetosEspeciais%\%NomeProjeto%-Beta"
		
	)
	if %ProjetoEspecial% == FALSE (
		echo Linha
		if not exist "%Projetos%\%Linha%\%NomeProjeto%-Beta" (
			echo criado
			echo "%Projetos%\%Linha%\%NomeProjeto%-Beta"
			MD "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		) else (
			::C�pia do ultimo arquivo
			CD "%Projetos%\%Linha%\%NomeProjeto%-Beta"
			if not exist "%Projetos%\%Linha%\%NomeProjeto%-Beta\OLD" (
				echo criado pasta de backup
				echo "%Projetos%\%Linha%\%NomeProjeto%-Beta\OLD"
				MD "%Projetos%\%Linha%\%NomeProjeto%-Beta\OLD"
			)
			7z a -tzip "%NomeProjeto% %datef% ; %timef%.zip" *.hex *.txt
			
			move "*.zip" "%Projetos%\%Linha%\%NomeProjeto%-Beta\OLD"
			CD "%CurrentDir%"
		)
		copy COMPILADO\%NomeArquivo%.hex "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		copy versao.txt "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		copy variavel.bat "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		copy Info.xlsx "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		copy %atalho%\Prog.bat "%Projetos%\%Linha%\%NomeProjeto%-Beta"
		copy %atalho%\Read.bat "%Projetos%\%Linha%\%NomeProjeto%-Beta"
	)
)
if %Parametro% == p call prog.bat


::echo .
::echo Fim
::echo .

	
	