@echo off

copy NUL P:\commitOK.txt
echo Already up to date.> P:\pullOK.txt

SETLOCAL ENABLEDELAYEDEXPANSION
for /r p:\ %%a in (*) do (
 if "%%~nxa"=="config" (
  set test="%%~dpnxa"
  set str=!test:\.git\config=!
  echo !str!
  git --git-dir=!str!\.git --work-tree=!str!\ pull > P:\pull.txt
  git --git-dir=!str!\.git --work-tree=!str!\ status -s > P:\commit.txt
  fc P:\commit.txt P:\commitOK.txt > nul
  if errorlevel 1 (
	cls
	echo/
	echo/
	echo !str!
	echo/
	echo/
	git --git-dir=!str!\.git --work-tree=!str!\ status
	echo/
	echo/
	pause
	)
  fc P:\pull.txt P:\pullOK.txt > nul
  if errorlevel 1 (
	cls
	echo/
	echo/
	echo !str!
	echo/
	echo/
	git --git-dir=!str!\.git --work-tree=!str!\ pull
	echo/
	echo/
	pause
	)
	
	del P:\commit.txt
	del P:\pull.txt
  )
 )
 
del P:\commitOK.txt
del P:\pullOK.txt
