@echo off
color F0
call variavel.bat

::PIC16
::ccsc -D -J -M +EX +EW +EA +P02 %NomeArquivo%


if %Compiler% == PIC18 (
::PIC18
ccsc -D -J -M +FH +EX +EW +EA +P02 %NomeArquivo%
) else if %Compiler% == PIC16 (
::PIC16
ccsc -D -J -M +EX +EW +EA +P02 %NomeArquivo%
)

type %NomeArquivo%.err

if exist "COMPILADO" (
	del COMPILADO\%NomeArquivo%.err
	del COMPILADO\%NomeArquivo%.hex
	)
if not exist "COMPILADO" (
	echo criado
	MD "COMPILADO"
	)

move %NomeArquivo%.err COMPILADO
:: move %NomeArquivo%.ccspjt COMPILADO
:: move %NomeArquivo%.cod COMPILADO
:: move %NomeArquivo%.sym COMPILADO
move %NomeArquivo%.lst COMPILADO
move %NomeArquivo%.hex COMPILADO